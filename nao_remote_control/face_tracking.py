#! /usr/bin/env python
# -*- encoding: UTF-8 -*-

"""Example: Use Tracking Module to Track a Face"""

import qi
import argparse
import sys
import time

def start_tracker(ip, port):
    session = init_session(ip, port)
    tracker = main(session, 0.1)
    return tracker

def main(session, faceSize):
    """
    This example shows how to use ALTracker with face.
    """
    tracker_service = session.service("ALTracker")

    # First, wake up.
    posture_service = session.service("ALRobotPosture")
    posture_service.goToPosture("Crouch", 1.0)
    #motion_service.setBreathEnabled("Body",True)

    # Add target to track.
    targetName = "Face"
    faceWidth = faceSize
    tracker_service.registerTarget(targetName, faceWidth)

    # Then, start tracker.
    tracker_service.track(targetName)

    print "ALTracker successfully started, now show your face to robot!"
    return tracker_service

def stop_tracker(tracker_service):
    tracker_service.stopTracker()
    tracker_service.unregisterAllTargets()
    print "ALTracker stopped."

def init_session(ip, port):
    session = qi.Session()
    session.connect("tcp://" + ip + ":" + str(port))
    return session

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="127.0.0.1",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")
    parser.add_argument("--facesize", type=float, default=0.1,
                        help="Face width.")

    args = parser.parse_args()
    session = qi.Session()
    try:
        session.connect("tcp://" + args.ip + ":" + str(args.port))
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
    main(session, args.facesize)
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        stop_tracker()
        print
        print "Interrupted by user"
        print "Stopping..."
