
import sys
from naoqi import ALProxy
from naoqi import motion
import init
import time
import threading

def openLeftHand():
    global motionProxy
    motionProxy.openHand("LHand")

def openRightHand():
    global motionProxy
    motionProxy.openHand("RHand")

def openHands():
    t1 = threading.Thread(target=openLeftHand)
    t2 = threading.Thread(target=openRightHand)
    t1.start()
    t2.start()
    t1.join()
    t2.join()

def closeLeftHand():
    global motionProxy
    motionProxy.closeHand("LHand")

def closeRightHand():
    global motionProxy
    motionProxy.closeHand("RHand")

def closeHands():
    t1 = threading.Thread(target=closeLeftHand)
    t2 = threading.Thread(target=closeRightHand)
    t1.start()
    t2.start()
    t1.join()
    t2.join()


def main(robotIP):
    global motionProxy
    PORT = 9559

    try:
        motionProxy = ALProxy("ALMotion", robotIP, PORT)
    except Exception,e:
        print "Could not create proxy to ALMotion"
        print "Error was: ",e
        sys.exit(1)

    try:
        postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
    except Exception, e:
        print "Could not create proxy to ALRobotPosture"
        print "Error was: ", e

    # Send NAO to Pose Init
    postureProxy.goToPosture("Crouch", 0.5)

    # Example of a cartesian foot trajectory
    # Warning: Needs a PoseInit before executing
    # Example available: path/to/aldebaran-sdk/examples/
    #                    python/motion_cartesianFoot.py

    space      =  motion.FRAME_TORSO
    axisMask   = 63                     # control all the effector's axes
    isAbsolute = True


    # Talking gesture
    effectorList = ["LArm", "RArm"]
    axisMaskList = [motion.AXIS_MASK_VEL, motion.AXIS_MASK_VEL]
    timeList     = [[1.0], [1.0]]         # seconds
    pathList     = [
                        [
                            [0.14931973814964294, 0.190760537981987, 0.02418871968984604, -3.1390016078948975, 0.03088182769715786, 0.43838971853256226]
                        ],
                        [
                            [0.1505744755268097, -0.16553089022636414, 0.0069661820307374, 3.0577945709228516, 0.11076471954584122, -0.3584825396537781]
                        ]
                    ]
    motionProxy.positionInterpolations(effectorList, space, pathList,
                                 axisMaskList, timeList, isAbsolute)

    openHands()


    effectorList = ["LArm", "RArm"]
    axisMaskList = [56,56]
    timeList     = [[1.0], [1.0]]         # seconds
    pathList     = [
                        [
                            [0.14931973814964294, 0.190760537981987, 0.02418871968984604, -3.1390016078948975, 0.03088182769715786, 0.43838971853256226]
                        ],
                        [
                            [0.1505744755268097, -0.16553089022636414, 0.0069661820307374, 3.0577945709228516, 0.11076471954584122, -0.3584825396537781]
                        ]
                    ]
    motionProxy.positionInterpolations(effectorList, space, pathList,
                                 axisMaskList, timeList, isAbsolute)

    time.sleep(5)
    closeHands()

if __name__ == "__main__":
    robotIp = "192.168.1.175"

    if len(sys.argv) <= 1:
        print "Usage python almotion_positioninterpolation.py robotIP (optional default: 127.0.0.1)"
    else:
        robotIp = sys.argv[1]

    main(robotIp)
    init.main(robotIp)
