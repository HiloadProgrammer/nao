import sys
from naoqi import ALProxy
from naoqi import motion
import init
import time

def main(robotIP):
    PORT = 9559

    try:
        motionProxy = ALProxy("ALMotion", robotIP, PORT)
    except Exception,e:
        print "Could not create proxy to ALMotion"
        print "Error was: ",e
        sys.exit(1)

    try:
        postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
    except Exception, e:
        print "Could not create proxy to ALRobotPosture"
        print "Error was: ", e

    # Send NAO to Pose Init
    postureProxy.goToPosture("Crouch", 0.5)

    # Example of a cartesian foot trajectory
    # Warning: Needs a PoseInit before executing
    # Example available: path/to/aldebaran-sdk/examples/
    #                    python/motion_cartesianFoot.py

    space      =  motion.FRAME_TORSO
    axisMask   = 63                     # control all the effector's axes
    isAbsolute = True

    # Point at something
    effector = "LArm"
    path = [0.1790427565574646, 0.08656293898820877, -0.024790169671177864, -1.205417513847351, 0.5693408250808716, -0.09484072029590607]

    timeList = 1.0 # seconds
    motionProxy.positionInterpolation(effector, space, path,
                                axisMask, timeList, isAbsolute)

    motionProxy.openHand("LHand")
    time.sleep(3)
    motionProxy.closeHand("LHand")
    # move left arm to a position so that it wont hurt itself
    effector = "LArm"
    path = [0.12063939869403839, 0.14973637461662292, -0.03792945668101311, -1.6440047025680542, 0.4670512080192566, 0.06774542480707169]

    timeList = 1.0 # seconds
    motionProxy.positionInterpolation(effector, space, path,
                                axisMask, timeList, isAbsolute)


if __name__ == "__main__":
    robotIp = "192.168.1.175"

    if len(sys.argv) <= 1:
        print "Usage python almotion_positioninterpolation.py robotIP (optional default: 127.0.0.1)"
    else:
        robotIp = sys.argv[1]

    main(robotIp)
    init.main(robotIp)
