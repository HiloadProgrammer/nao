import sys
from naoqi import ALProxy
from naoqi import motion
import init
import time

def main(robotIP):
    PORT = 9559

    try:
        motionProxy = ALProxy("ALMotion", robotIP, PORT)
    except Exception,e:
        print "Could not create proxy to ALMotion"
        print "Error was: ",e
        sys.exit(1)

    try:
        postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
    except Exception, e:
        print "Could not create proxy to ALRobotPosture"
        print "Error was: ", e

    # Send NAO to Pose Init
    postureProxy.goToPosture("Crouch", 0.5)

    # Example of a cartesian foot trajectory
    # Warning: Needs a PoseInit before executing
    # Example available: path/to/aldebaran-sdk/examples/
    #                    python/motion_cartesianFoot.py

    space      =  motion.FRAME_TORSO
    axisMask   = 63                     # control all the effector's axes
    isAbsolute = True


    motionProxy.openHand("LHand")
    # wave left arm
    effector = "LArm"
    path = [
                [0.07982008904218674, 0.09168355166912079, 0.25744152069091797, 1.5274202823638916, -1.147329568862915, -1.846019983291626],
                [0.06546352058649063, 0.22919462621212006, 0.22828194499015808, -1.6147031784057617, -1.1824967861175537, 1.1901507377624512],
                [0.07982008904218674, 0.09168355166912079, 0.25744152069091797, 1.5274202823638916, -1.147329568862915, -1.846019983291626],
                [0.06546352058649063, 0.22919462621212006, 0.22828194499015808, -1.6147031784057617, -1.1824967861175537, 1.1901507377624512]
            ]

    timeList = [0.5, 1.0, 1.5, 2.0] # seconds
    motionProxy.positionInterpolation(effector, space, path,
                                axisMask, timeList, isAbsolute)

    motionProxy.closeHand("LHand")

if __name__ == "__main__":
    robotIp = "192.168.1.175"

    if len(sys.argv) <= 1:
        print "Usage python almotion_positioninterpolation.py robotIP (optional default: 127.0.0.1)"
    else:
        robotIp = sys.argv[1]

    main(robotIp)
    init.main(robotIp)
