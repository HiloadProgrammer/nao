
import sys
from naoqi import ALProxy
from naoqi import motion
import init
import time
import threading

def openLeftHand(motionProxy):
    motionProxy.openHand("LHand")

def openRightHand(motionProxy):
    motionProxy.openHand("RHand")

def openHands(motionProxy):
    t1 = threading.Thread(target=openLeftHand, args=[motionProxy])
    t2 = threading.Thread(target=openRightHand, args=[motionProxy])
    t1.start()
    t2.start()
    t1.join()
    t2.join()

def closeLeftHand(motionProxy):
    motionProxy.closeHand("LHand")

def closeRightHand(motionProxy):
    motionProxy.closeHand("RHand")

def closeHands(motionProxy):
    t1 = threading.Thread(target=closeLeftHand, args=[motionProxy])
    t2 = threading.Thread(target=closeRightHand, args=[motionProxy])
    t1.start()
    t2.start()
    t1.join()
    t2.join()
