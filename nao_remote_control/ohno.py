import sys
from naoqi import ALProxy
from naoqi import motion
import init
import time
import hand_gestures as hands

def main(robotIP):
    PORT = 9559

    try:
        motionProxy = ALProxy("ALMotion", robotIP, PORT)
    except Exception,e:
        print "Could not create proxy to ALMotion"
        print "Error was: ",e
        sys.exit(1)

    try:
        postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
    except Exception, e:
        print "Could not create proxy to ALRobotPosture"
        print "Error was: ", e

    # Send NAO to Pose Init
    postureProxy.goToPosture("Crouch", 0.5)

    # Example of a cartesian foot trajectory
    # Warning: Needs a PoseInit before executing
    # Example available: path/to/aldebaran-sdk/examples/
    #                    python/motion_cartesianFoot.py

    space      =  motion.FRAME_TORSO
    axisMask   = 63                     # control all the effector's axes
    isAbsolute = True




    # save position
    effectorList = ["LArm", "RArm"]
    axisMaskList = [motion.AXIS_MASK_VEL, motion.AXIS_MASK_VEL]
    timeList     = [[1.0], [1.0]]         # seconds
    pathList     = [
                        [
                            [0.1722925901412964, 0.12734992802143097, 0.040642399340867996, -0.9585068225860596, -0.12930868566036224, -0.13844828307628632],
                        ],
                        [

                            [0.18271535634994507, -0.12039097398519516, 0.058854810893535614, 0.6934764981269836, -0.2884313464164734, 0.11263268440961838],
                        ]
                    ]
    motionProxy.positionInterpolations(effectorList, space, pathList,
                                 axisMaskList, timeList, isAbsolute)


    hands.openHands(motionProxy)

    # Oh Gott oh Gott gesture
    effectorList = ["LArm", "RArm"]
    axisMaskList = [motion.AXIS_MASK_VEL, motion.AXIS_MASK_VEL]
    timeList     = [[0.5], [0.5]]         # seconds
    pathList     = [
                        [
                            [0.06622066348791122, 0.05250571295619011, 0.23846064507961273, -0.8418099284172058, -1.1334611177444458, -2.020690679550171]
                        ],
                        [
                            [0.05924132466316223, -0.05834430456161499, 0.2302331179380417, 0.10343576222658157, -1.1975678205490112, 2.4804794788360596]
                        ]
                    ]
    motionProxy.positionInterpolations(effectorList, space, pathList,
                                 axisMaskList, timeList, isAbsolute)


    # Oh Gott oh Gott gesture rotate writs
    effectorList = ["LArm", "RArm"]
    axisMaskList = [56, 56]
    timeList     = [[0.5], [0.5]]         # seconds
    pathList     = [
                        [
                            [0.06622066348791122, 0.05250571295619011, 0.23846064507961273, -0.8418099284172058, -1.1334611177444458, -2.020690679550171]
                        ],
                        [
                            [0.05924132466316223, -0.05834430456161499, 0.2302331179380417, 0.10343576222658157, -1.1975678205490112, 2.4804794788360596]

                        ]
                    ]
    motionProxy.positionInterpolations(effectorList, space, pathList,
                                 axisMaskList, timeList, isAbsolute)
    hands.closeHands(motionProxy)
    hands.openHands(motionProxy)
    time.sleep(3)
    # save position
    effectorList = ["LArm", "RArm"]
    axisMaskList = [motion.AXIS_MASK_VEL, motion.AXIS_MASK_VEL]
    timeList     = [[0.5], [0.5]]         # seconds
    pathList     = [
                        [
                            [0.1722925901412964, 0.12734992802143097, 0.040642399340867996, -0.9585068225860596, -0.12930868566036224, -0.13844828307628632]
                        ],
                        [
                            [0.18271535634994507, -0.12039097398519516, 0.058854810893535614, 0.6934764981269836, -0.2884313464164734, 0.11263268440961838]
                        ]
                    ]
    motionProxy.positionInterpolations(effectorList, space, pathList,
                                 axisMaskList, timeList, isAbsolute)

    hands.closeHands(motionProxy)

if __name__ == "__main__":
    robotIp = "192.168.1.175"

    if len(sys.argv) <= 1:
        print "Usage python almotion_positioninterpolation.py robotIP (optional default: 127.0.0.1)"
    else:
        robotIp = sys.argv[1]

    main(robotIp)
    init.main(robotIp)
