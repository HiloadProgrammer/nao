import sys
from naoqi import ALProxy
from naoqi import motion
import init
import time
import hand_gestures as hands

def main_arm_inside(robotIP):
    function_name = "main_arm_inside"
    PORT = 9559

    try:
        motionProxy = ALProxy("ALMotion", robotIP, PORT)
    except Exception,e:
        print "%s: Could not create proxy to ALMotion" % (function_name)
        print "Error was: ",e
        sys.exit(1)

    try:
        postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
    except Exception, e:
        print "%s: Could not create proxy to ALRobotPosture" % (function_name)
        print "Error was: ", e



    # Example of a cartesian foot trajectory
    # Warning: Needs a PoseInit before executing
    # Example available: path/to/aldebaran-sdk/examples/
    #                    python/motion_cartesianFoot.py

    space      =  motion.FRAME_TORSO
    axisMask   = 63                     # control all the effector's axes
    isAbsolute = True




    # save position
    effectorList = ["RArm", "LArm"]
    axisMaskList = [motion.AXIS_MASK_VEL, motion.AXIS_MASK_VEL]
    timeList     = [[2.0], [2.0]]         # seconds
    pathList     = [
                        [
                            [0.09688302874565125, -0.05057424679398537, -0.04766279458999634, 1.3315329551696777, 0.384397029876709, 0.6029329895973206],
                        ],
                        [
                            [0.08870188146829605, 0.04995298385620117, -0.05269230902194977, -1.3126620054244995, 0.4309654235839844, -0.6543590426445007],
                        ]
                    ]
    motionProxy.positionInterpolations(effectorList, space, pathList,
                                 axisMaskList, timeList, isAbsolute)

def main_arm_init(robotIP):
    funciton_name = "main_arm_init"
    PORT = 9559

    try:
        motionProxy = ALProxy("ALMotion", robotIP, PORT)
    except Exception,e:
        print "%s: Could not create proxy to ALMotion" % (function_name)
        print "Error was: ",e
        sys.exit(1)

    try:
        postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
    except Exception, e:
        print "%s: Could not create proxy to ALRobotPosture" % (function_name)
        print "Error was: ", e



    # Example of a cartesian foot trajectory
    # Warning: Needs a PoseInit before executing
    # Example available: path/to/aldebaran-sdk/examples/
    #                    python/motion_cartesianFoot.py

    space      =  motion.FRAME_TORSO
    axisMask   = 63                     # control all the effector's axes
    isAbsolute = True




    # save position
    effectorList = ["RArm", "LArm"]
    axisMaskList = [motion.AXIS_MASK_VEL, motion.AXIS_MASK_VEL]
    timeList     = [[2.0], [2.0]]         # seconds
    pathList     = [
                        [
                            [0.08670857548713684, -0.040713973343372345, -0.05058114230632782, 1.2754532098770142, 0.40013617277145386, 0.714888870716095],
                        ],
                        [
                            [0.08296854794025421, 0.04060589149594307, -0.05305814743041992, -1.4218592643737793, 0.43736472725868225, -0.7247690558433533],
                        ]
                    ]
    motionProxy.positionInterpolations(effectorList, space, pathList,
                                 axisMaskList, timeList, isAbsolute)


def main_arm_outside(robotIP):
    funciton_name = "main_arm_outside"
    PORT = 9559

    try:
        motionProxy = ALProxy("ALMotion", robotIP, PORT)
    except Exception,e:
        print "%s: Could not create proxy to ALMotion" % (function_name)
        print "Error was: ",e
        sys.exit(1)

    try:
        postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
    except Exception, e:
        print "%s: Could not create proxy to ALRobotPosture" % (function_name)
        print "Error was: ", e



    # Example of a cartesian foot trajectory
    # Warning: Needs a PoseInit before executing
    # Example available: path/to/aldebaran-sdk/examples/
    #                    python/motion_cartesianFoot.py

    space      =  motion.FRAME_TORSO
    axisMask   = 63                     # control all the effector's axes
    isAbsolute = True




    # save position
    effectorList = ["RArm", "LArm"]
    axisMaskList = [motion.AXIS_MASK_VEL, motion.AXIS_MASK_VEL]
    timeList     = [[2.0], [2.0]]         # seconds
    pathList     = [
                        [
                            [0.08861583471298218, -0.037142567336559296, -0.04874194785952568, 1.259790062904358, 0.37734508514404297, 0.713744044303894],
                        ],
                        [
                            [0.08749493211507797, 0.0429309606552124, -0.050512202084064484, -1.3925597667694092, 0.41348057985305786, -0.6963844299316406],
                         ]
                    ]
    motionProxy.positionInterpolations(effectorList, space, pathList,
                                 axisMaskList, timeList, isAbsolute)


def main_torso_back(robotIP):
    funciton_name = "main_torso_back"
    PORT = 9559

    try:
        motionProxy = ALProxy("ALMotion", robotIP, PORT)
    except Exception,e:
        print "%s: Could not create proxy to ALMotion" % (function_name)
        print "Error was: ",e
        sys.exit(1)

    try:
        postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
    except Exception, e:
        print "%s: Could not create proxy to ALRobotPosture" % (function_name)
        print "Error was: ", e



    # Example of a cartesian foot trajectory
    # Warning: Needs a PoseInit before executing
    # Example available: path/to/aldebaran-sdk/examples/
    #                    python/motion_cartesianFoot.py

    space      =  motion.FRAME_TORSO
    axisMask   = 63                     # control all the effector's axes
    isAbsolute = True




    # save position
    effectorList = ["RLeg", "LLeg"]
    axisMaskList = [motion.AXIS_MASK_VEL, motion.AXIS_MASK_VEL]
    timeList     = [[2.0], [2.0]]         # seconds
    pathList     = [
                        [
                            [-0.03025922365486622, -0.03587617352604866, -0.2256138026714325, 0.011835718527436256, 0.11670533567667007, -0.15116837620735168],
                        ],
                        [
                            [-0.029549501836299896, 0.03633652254939079, -0.22579796612262726, -0.010086258873343468, 0.10915496945381165, 0.15274913609027863],
                        ]
                    ]
    motionProxy.positionInterpolations(effectorList, space, pathList,
                                 axisMaskList, timeList, isAbsolute)


def main_arm_all(robotIP):
    funciton_name = "main_arm_all"
    PORT = 9559

    try:
        motionProxy = ALProxy("ALMotion", robotIP, PORT)
    except Exception,e:
        print "%s: Could not create proxy to ALMotion" % (function_name)
        print "Error was: ",e
        sys.exit(1)

    try:
        postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
    except Exception, e:
        print "%s: Could not create proxy to ALRobotPosture" % (function_name)
        print "Error was: ", e



    # Example of a cartesian foot trajectory
    # Warning: Needs a PoseInit before executing
    # Example available: path/to/aldebaran-sdk/examples/
    #                    python/motion_cartesianFoot.py

    space      =  motion.FRAME_TORSO
    axisMask   = 63                     # control all the effector's axes
    isAbsolute = True




    # save position
    effectorList = ["RArm", "LArm"]
    axisMaskList = [motion.AXIS_MASK_VEL, motion.AXIS_MASK_VEL]
    timeList     = [[1.0, 3.0, 5.0, 7.0], [1.0, 3.0, 5.0, 7.0]]         # seconds
    pathList     = [
                        [
                            [0.08861583471298218, -0.037142567336559296, -0.04874194785952568, 1.259790062904358, 0.37734508514404297, 0.713744044303894],
                            [0.08670857548713684, -0.040713973343372345, -0.05058114230632782, 1.2754532098770142, 0.40013617277145386, 0.714888870716095],
                            [0.09688302874565125, -0.05057424679398537, -0.04766279458999634, 1.3315329551696777, 0.384397029876709, 0.6029329895973206],
                            [0.08670857548713684, -0.040713973343372345, -0.05058114230632782, 1.2754532098770142, 0.40013617277145386, 0.714888870716095]
                        ],
                        [
                            [0.08749493211507797, 0.0429309606552124, -0.050512202084064484, -1.3925597667694092, 0.41348057985305786, -0.6963844299316406],
                            [0.08296854794025421, 0.04060589149594307, -0.05305814743041992, -1.4218592643737793, 0.43736472725868225, -0.7247690558433533],
                            [0.08870188146829605, 0.04995298385620117, -0.05269230902194977, -1.3126620054244995, 0.4309654235839844, -0.6543590426445007],
                            [0.08296854794025421, 0.04060589149594307, -0.05305814743041992, -1.4218592643737793, 0.43736472725868225, -0.7247690558433533]
                        ]
                    ]
    motionProxy.positionInterpolations(effectorList, space, pathList,
                                 axisMaskList, timeList, isAbsolute)




def main_torso_init(robotIP):
    function_name = "main_torso_init"
    PORT = 9559

    try:
        motionProxy = ALProxy("ALMotion", robotIP, PORT)
    except Exception,e:
        print "%s: Could not create proxy to ALMotion" % (function_name)
        print "Error was: ",e
        sys.exit(1)

    try:
        postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
    except Exception, e:
        print "%s: Could not create proxy to ALRobotPosture" % (function_name)
        print "Error was: ", e



    # Example of a cartesian foot trajectory
    # Warning: Needs a PoseInit before executing
    # Example available: path/to/aldebaran-sdk/examples/
    #                    python/motion_cartesianFoot.py

    space      =  motion.FRAME_TORSO
    axisMask   = 63                     # control all the effector's axes
    isAbsolute = True




    # save position
    effectorList = ["RLeg", "LLeg"]
    axisMaskList = [motion.AXIS_MASK_VEL, motion.AXIS_MASK_VEL]
    timeList     = [[2.0], [2.0]]         # seconds
    pathList     = [
                        [
                            [-0.021319283172488213, -0.036472026258707047, -0.2273055911064148, 0.017424283549189568, 0.05396851524710655, -0.1557822823524475],
                        ],
                        [
                            [-0.021035701036453247, 0.036882806569337845, -0.22737418115139008, -0.01574505679309368, 0.049462124705314636, 0.15691575407981873],
                        ]
                    ]
    motionProxy.positionInterpolations(effectorList, space, pathList,
                                 axisMaskList, timeList, isAbsolute)

def main_torso_front(robotIP):
    funciton_name = "main_torso_front"
    PORT = 9559

    try:
        motionProxy = ALProxy("ALMotion", robotIP, PORT)
    except Exception,e:
        print "%s: Could not create proxy to ALMotion" % (function_name)
        print "Error was: ",e
        sys.exit(1)

    try:
        postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
    except Exception, e:
        print "%s: Could not create proxy to ALRobotPosture" % (function_name)
        print "Error was: ", e



    # Example of a cartesian foot trajectory
    # Warning: Needs a PoseInit before executing
    # Example available: path/to/aldebaran-sdk/examples/
    #                    python/motion_cartesianFoot.py

    space      =  motion.FRAME_TORSO
    axisMask   = 63                     # control all the effector's axes
    isAbsolute = True




    # save position
    effectorList = ["RLeg", "LLeg"]
    axisMaskList = [motion.AXIS_MASK_VEL, motion.AXIS_MASK_VEL]
    timeList     = [[2.0], [2.0]]         # seconds
    pathList     = [
                        [
                            [-0.011444875039160252, -0.037052661180496216, -0.22849141061306, 0.02351222187280655, -0.014396307989954948, -0.16221921145915985],
                        ],
                        [
                            [-0.010770836845040321, 0.037820640951395035, -0.22859522700309753, -0.01882786862552166, -0.021947115659713745, 0.16370375454425812],
                        ]
                    ]
    motionProxy.positionInterpolations(effectorList, space, pathList,
                                 axisMaskList, timeList, isAbsolute)

def main_torso_all(robotIP):
    funciton_name = "main_torso_all"
    PORT = 9559

    try:
        motionProxy = ALProxy("ALMotion", robotIP, PORT)
    except Exception,e:
        print "%s: Could not create proxy to ALMotion" % (function_name)
        print "Error was: ",e
        sys.exit(1)

    try:
        postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
    except Exception, e:
        print "%s: Could not create proxy to ALRobotPosture" % (function_name)
        print "Error was: ", e



    # Example of a cartesian foot trajectory
    # Warning: Needs a PoseInit before executing
    # Example available: path/to/aldebaran-sdk/examples/
    #                    python/motion_cartesianFoot.py

    space      =  motion.FRAME_TORSO
    axisMask   = 63                     # control all the effector's axes
    isAbsolute = True




    # save position
    effectorList = ["RLeg", "LLeg"]
    axisMaskList = [motion.AXIS_MASK_VEL, motion.AXIS_MASK_VEL]
    timeList     = [[1.0, 3.0, 5.0, 7.0], [1.0, 3.0, 5.0, 7.0]]         # seconds
    pathList     = [
                        [
                            [-0.011444875039160252, -0.037052661180496216, -0.22849141061306, 0.02351222187280655, -0.014396307989954948, -0.16221921145915985],
                            [-0.021319283172488213, -0.036472026258707047, -0.2273055911064148, 0.017424283549189568, 0.05396851524710655, -0.1557822823524475],
                            [-0.03025922365486622, -0.03587617352604866, -0.2256138026714325, 0.011835718527436256, 0.11670533567667007, -0.15116837620735168],
                            [-0.021319283172488213, -0.036472026258707047, -0.2273055911064148, 0.017424283549189568, 0.05396851524710655, -0.1557822823524475]
                        ],
                        [
                            [-0.010770836845040321, 0.037820640951395035, -0.22859522700309753, -0.01882786862552166, -0.021947115659713745, 0.16370375454425812],
                            [-0.021035701036453247, 0.036882806569337845, -0.22737418115139008, -0.01574505679309368, 0.049462124705314636, 0.15691575407981873],
                            [-0.029549501836299896, 0.03633652254939079, -0.22579796612262726, -0.010086258873343468, 0.10915496945381165, 0.15274913609027863],
                            [-0.021035701036453247, 0.036882806569337845, -0.22737418115139008, -0.01574505679309368, 0.049462124705314636, 0.15691575407981873]
                        ]
                    ]
    motionProxy.positionInterpolations(effectorList, space, pathList,
                                 axisMaskList, timeList, isAbsolute)

def main_eyes(robotIP):
    funciton_name = "main_eyes"
    PORT = 9559
    try:
        ledProxy = ALProxy("ALLeds", robotIP, PORT)
    except Exception,e:
        print "%s: Could not create proxy to ALLeds" % (function_name)
        print "Error was: ",e
        sys.exit(1)

    ledProxy.post.fadeRGB("FaceLed0", 0x000000, 0.02)
    ledProxy.post.fadeRGB("FaceLed1", 0x000000, 0.02)
    ledProxy.post.fadeRGB("FaceLed2", 0xffffff, 0.02)
    ledProxy.post.fadeRGB("FaceLed3", 0x000000, 0.02)
    ledProxy.post.fadeRGB("FaceLed4", 0x000000, 0.02)
    ledProxy.post.fadeRGB("FaceLed5", 0x000000, 0.02)
    ledProxy.post.fadeRGB("FaceLed6", 0xffffff, 0.02)
    ledProxy.fadeRGB("FaceLed7", 0x000000, 0.02)
    time.sleep(0.1)
    ledProxy.fadeRGB("FaceLeds", 0xffffff, 0.02)

if __name__ == "__main__":
    robotIp = "192.168.1.175"

    if len(sys.argv) <= 1:
        print "Usage python almotion_positioninterpolation.py robotIP (optional default: 127.0.0.1)"
    else:
        robotIp = sys.argv[1]

    main_torso(robotIp)
    main_arm(robotIp)
    main_eyes(robotIp)
    init.main(robotIp)
