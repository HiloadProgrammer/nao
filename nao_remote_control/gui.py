#Script needed: naoqi from Aldebaran & tkinter (ubuntu package)

from Tkinter import *
from naoqi import ALProxy
import threading
import ohno
import point
import wave
import talk
import natural_gesture
import sys
import face_tracking

root = Tk()
lastText = ""
var2 = IntVar()
session = None

#nao proxys
#'''
robotIP = "192.168.1.117"
PORT = 9559
natural_online = False
current_Action = False
exiting = False
natural = None
track_face = False
face_tracker = None


# doing some little movements while its not moving to act natural if nao isn't moving at all
def act_natural():
    global natural_online
    global robotIP
    global current_Action
    global exiting
    while exiting is False:
        while natural_online is True and exiting is False:
	    print "act natural"
            if(natural_online is True):
		current_Action = True
                natural_gesture.main_torso_front(robotIP)
		current_Action = False
            if(natural_online is True):
		current_Action = True
                natural_gesture.main_torso_init(robotIP)
		current_Action = False
            if(natural_online is True):
		current_Action = True
                natural_gesture.main_torso_back(robotIP)
		current_Action = False
            if(natural_online is True):
		current_Action = True
                natural_gesture.main_torso_init(robotIP)
		current_Action = False
            """
            if(natural_online is True):
                natural_gesture.main_arm_outside(robotIP)
            if(natural_online is True):
                natural_gesture.main_arm_init(robotIP)
            if(natural_online is True):
                natural_gesture.main_arm_inside(robotIP)
            if(natural_online is True):
                natural_gesture.main_arm_init(robotIP)
            """

    print "end natural thread"

# locking the movement for a gesture or posture (stops act_natural)
def lock_movement():
    global natural_online
    global current_Action
    global exiting
    natural_online=False
    while(current_Action and exiting is False):
	       pointless = True

# give the movement free for natural behavior
def free_movement():
    global var2
    if(var2.get()==1):
        global natural_online
        natural_online=True
        print "movement free"

def lock_gesture_wink():
    lock_movement()
    wave.main(robotIP)
    free_movement()

def gesture_wink():
    print "*winks*"
    g1 = threading.Thread(target=lock_gesture_wink)
    g1.start()

def lock_gesture_point():
    lock_movement()
    point.main(robotIP)
    free_movement()

def gesture_point():
    print "*point*"
    g2 = threading.Thread(target=lock_gesture_point)
    g2.start()

def lock_gesture_talk():
    lock_movement()
    talk.main(robotIP)
    free_movement()

def gesture_talk():
    print "*talk gesture*"
    g3 = threading.Thread(target=lock_gesture_talk)
    g3.start()

def lock_gesture_panic():
    global robotIP
    lock_movement()
    ohno.main(robotIP)
    free_movement()

def gesture_panic():
    global robotIP
    print "*panic*"
    g4 = threading.Thread(target=lock_gesture_panic)
    g4.start()

def talk_enter(event):
    talk_say()

def talk_say():
    global lastText
    lastText = entry.get()
    print "*nao say* : " + lastText
    s1 = threading.Thread(target=tts.say, args=[lastText])
    s1.start()
    entry.delete(0,END)

def talk_greet():
    entry.delete(0, END)
    #entry.insert(0,"Hello I'm nao, how are you?")
    entry.insert(0,"Hallo ich bin Nao. Ein Humanoider Roboter. Wer bist du?")
    talk_say()

def talk_bye():
    entry.delete(0, END)
    #entry.insert(0,"It was a pleasure to talk to you")
    entry.insert(0,"Es hat mich gefreut mit dir zu reden.")
    talk_say()

def talk_point():
    entry.delete(0, END)
    #entry.insert(0,"Could you please fill out this form for me?")
    entry.insert(0,"Kannst du bitte dieses Formular fuer mich ausfuellen?")
    talk_say()

def talk_panic():
    entry.delete(0, END)
    #entry.insert(0,"Oh no!! He has my hat!!")
    entry.insert(0,"Oh NEIN, Er hat meinen Hut geklaut!")
    talk_say()

def talk_yes():
    entry.delete(0, END)
    #entry.insert(0,"Yes of course")
    entry.insert(0,"Natuerlich")
    talk_say()

def talk_no():
    entry.delete(0, END)
    #entry.insert(0,"No")
    entry.insert(0,"Nein")
    talk_say()

def talk_think():
    entry.delete(0, END)
    #entry.insert(0,"Let me think about that")
    entry.insert(0,"Lass mich kurz nachdenken.")
    talk_say()

def talk_story():
    entry.delete(0, END)
    #entry.insert(0,"Can you tell me a story?")
    entry.insert(0,"Kannst du mir eine Geschichte erzaehlen?")
    talk_say()

def talk_color():
    entry.delete(0, END)
    #entry.insert(0,"Whats your favourite color?")
    entry.insert(0,"Welche Farbe ist deine Lieblingsfarbe?")
    talk_say()


def talk_general(text):
    entry.delete(0, END)
    entry.insert(0,text)
    talk_say()

def face_tracking_on():
    global session
    global face_tracker
    global track_face
    if track_face is True:
        if session is None:
            session = face_tracking.init_session(robotIP, PORT)
            face_tracker = face_tracking.main(session, 0.1)

def face_tracking_off():
    global session
    global face_tracker
    if face_tracker is not None:
        face_tracking.stop_tracker(face_tracker)
    session = None

def toggle_face_tracking():
    pointless = True

def toggle_natural():
    global natural_online
    global natural
    if(var2.get()==1):
       print "natural behavior on"
       natural_online = True
       if natural is None:
           natural = threading.Thread(target=act_natural)
           natural.start()
    else:
       print "natural behavior off"
       natural_online = False

def gesture_rest():
    print "putting nao to sleep"
    lock_movement()
    motionProxy.rest()
    free_movement()

def posture_sit():
    print "nao sit"
    lock_movement()
    t1 = threading.Thread(target=postureProxy.goToPosture, args=["Sit",0.5])
    t1.start()
    t1.join()
    free_movement()

def posture_crouch():
    lock_movement()
    t2 = threading.Thread(target=postureProxy.goToPosture, args=["Crouch",0.5])
    t2.start()
    t2.join()
    free_movement()

def posture_stand():
    lock_movement()
    t3 = threading.Thread(target=postureProxy.goToPosture, args=["Stand",0.5])
    t3.start()
    t4.join()
    free_movement()

def posture_relax():
    lock_movement()
    t4 = threading.Thread(target=postureProxy.goToPosture, args=["SitRelax",0.5])
    t4.start()
    t4.join()
    free_movement()

def load_last(event):
    entry.delete(0, END)
    entry.insert(0,lastText)

def key_in(event):
    if(event.char == '1'):
        gesture_wink()
    elif(event.char == '2'):
        gesture_point()
    elif(event.char == '3'):
        gesture_panic()
    elif(event.char == '4'):
        gesture_talk()
    elif(event.char == '5'):
        talk_bye()
    elif(event.char == '6'):
        talk_point()
    elif(event.char == '7'):
        talk_panic()
    elif(event.char == '8'):
        talk_yes()
    elif(event.char == '9'):
        talk_no()
    elif(event.char == '0'):
        talk_think()
    elif(event.char == '+'):
        posture_sit()
    elif(event.char == '#'):
        posture_crouch()

def close_gui():
    global exiting
    global natural
    exiting = True
    if natural is not None:
        natural.join()
    quit()
    #natural.stop()
    #sys.exit(0)

#key listeners
root.bind('<Up>', load_last)
root.bind_all('<Key>', key_in)


var1 = IntVar()
Checkbutton(root, text='Toggle Facetracking', command=toggle_face_tracking, variable=var1).grid(row=0, column=0)


Checkbutton(root, text="Act natural", variable=var2, command=toggle_natural).grid(row=0, column=1,sticky=W)

Button(root, text='Rest', command=gesture_rest).grid(row=0, column=2, sticky=W)


logo = PhotoImage(file="NAO2.gif")
logo_label = Label(root, image=logo).grid(row=2, column=8)



Label(root, text="Gestures").grid(row=1, column=0)
Button(root, text='Wink(1)', command=gesture_wink).grid(row=1, column=2)
Button(root, text='Point(2)', command=gesture_point).grid(row=1, column=3)
Button(root, text='Panic(3)', command=gesture_panic).grid(row=1, column=4)
Button(root, text='Talk(4)', command=gesture_talk).grid(row=1, column=5)




Label(root, text='Talk: ').grid(row=2, column=0)
entry = Entry(root)
entry.bind("<Return>", talk_enter)
entry.grid(row=2, column=1)
Button(root, text='Send',command=talk_say).grid(row=2, column=2)




Label(root, text='Quicktalk: ').grid(row=3, column=0)
Button(root, text='Greet',command=talk_greet).grid(row=3, column=2)
Button(root, text='Bye(5)',command=talk_bye).grid(row=3, column=3)
Button(root, text='Point(6)',command=talk_point).grid(row=3, column=4)
Button(root, text='Panic(7)',command=talk_panic).grid(row=3, column=5)
Button(root, text='Yes(8)', command=talk_yes).grid(row=3, column=6)
Button(root, text='No(9)', command=talk_no).grid(row=3, column=7)
Button(root, text='Think(0)', command=talk_think).grid(row=3, column=8)
Button(root, text='Story', command=talk_story).grid(row=3, column=9)
Button(root, text='Color', command=talk_color).grid(row=3, column=10)
Button(root, text='Color meine auch', command=lambda: talk_general('Das ist auch meine Lieblingsfarbe.')).grid(row=3, column=11)

#Button(root, text='Interesting', command=lambda: talk_general('Thats Interesting')).grid(row=4, column=2)
Button(root, text='Interesting', command=lambda: talk_general('Interessant')).grid(row=4, column=2)
#Button(root, text='Nice', command=lambda: talk_general('Nice')).grid(row=4, column=3)
Button(root, text='Nice', command=lambda: talk_general('Sehr gut')).grid(row=4, column=3)
#Button(root, text='Perfect', command=lambda: talk_general('Perfect')).grid(row=4, column=4)
Button(root, text='Perfect', command=lambda: talk_general('Perfekt')).grid(row=4, column=4)
Button(root, text='Super', command=lambda: talk_general('Super')).grid(row=4, column=5)
Button(root, text='Ok', command=lambda: talk_general('OK')).grid(row=4, column=6)
#Button(root, text='Great', command=lambda: talk_general('Great')).grid(row=4, column=7)
Button(root, text='Great', command=lambda: talk_general('Grosartig')).grid(row=4, column=7)
#Button(root, text='Accepted', command=lambda: talk_general('Accepted')).grid(row=4, column=8)
Button(root, text='Accepted', command=lambda: talk_general('Verstanden')).grid(row=4, column=8)
#Button(root, text='How was your day?', command=lambda: talk_general('How was your day?')).grid(row=4, column=9)
Button(root, text='How was your day?', command=lambda: talk_general('Wie war dein Tag?')).grid(row=4, column=9)
#Button(root, text='Update DB', command=lambda: talk_general('Could you help me update my database? I would like to work in a Supermarket. Could you name some fruits?')).grid(row=4, column=10)
Button(root, text='Update DB', command=lambda: talk_general('Koenntest du mir bitte helfen meine Datenbank zu aktualisieren? Ich moechte in einem Supermarkt arbeiten. Koenntest du mir ein paar Gemuesesorten nennen?')).grid(row=4, column=10)
Button(root, text='Das reicht', command=lambda: talk_general('Danke, ich habe jetzt genuegend Informationen gesammelt.')).grid(row=4, column=11)


Label(root, text='Postures:').grid(row=5, column=0)
Button(root, text='Sit(+)', command=posture_sit).grid(row=5, column=2)
Button(root, text='Crouch(#)', command=posture_crouch).grid(row=5, column=3)
Button(root, text='Stand', command=posture_stand).grid(row=5, column=4)
Button(root, text='Relax', command=posture_relax).grid(row=5, column=5)


Button(root, text='Close', command=close_gui).grid(row=5, column=6)

try:

    #blink = threading.Thread(target=natural_gesture.main_eyes, args=[robotIP]);
    #blink.start()

    tts = ALProxy("ALTextToSpeech", robotIP, PORT)
    tts.setLanguage("German")
    motionProxy = ALProxy("ALMotion", robotIP, PORT)

    postureProxy = ALProxy("ALRobotPosture", robotIP, PORT)
    #'''

    root.protocol("WM_DELETE_WINDOW", close_gui)

    mainloop()
except Exception, e:
    print "#Could not create proxy to ALRobotPosture"
    print "Error was: ", e
    close_gui()
